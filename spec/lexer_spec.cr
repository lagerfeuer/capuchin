require "./spec_helper"
require "../src/capuchin/lexer/lexer.cr"
require "../src/capuchin/token/token.cr"

include Capuchin

describe Lexer do
  it "#nextToken symbols/operators/delimiters" do
    input = "+-*/=!()[]{},;:"
    ref = [
      default_token(TokenType::PLUS),
      default_token(TokenType::MINUS),
      default_token(TokenType::ASTERISK),
      default_token(TokenType::SLASH),
      default_token(TokenType::ASSIGN),
      default_token(TokenType::BANG),
      default_token(TokenType::LPAREN),
      default_token(TokenType::RPAREN),
      default_token(TokenType::LBRACKET),
      default_token(TokenType::RBRACKET),
      default_token(TokenType::LBRACE),
      default_token(TokenType::RBRACE),
      default_token(TokenType::COMMA),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::COLON),
      default_token(TokenType::EOF),
    ]
    assert_tokens(Lexer.new(input), ref)
  end

  it "#nextToken example code" do
    input = <<-EOF
let five = 5;
let ten = 10;
let bool = true;
let string = "hello";

fun add(x, y) {
  x + y;
};
let result = add(five, ten);

if (result >= 0) {
  return true;
} else {
  return result != 50;
}

return false;
EOF

    ref = [
      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "five"),
      default_token(TokenType::ASSIGN),
      Token.new(TokenType::INTEGER, "5"),
      default_token(TokenType::SEMICOLON),

      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "ten"),
      default_token(TokenType::ASSIGN),
      Token.new(TokenType::INTEGER, "10"),
      default_token(TokenType::SEMICOLON),

      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "bool"),
      default_token(TokenType::ASSIGN),
      default_token(TokenType::TRUE),
      default_token(TokenType::SEMICOLON),

      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "string"),
      default_token(TokenType::ASSIGN),
      Token.new(TokenType::STRING, %("hello")),
      default_token(TokenType::SEMICOLON),

      default_token(TokenType::FUNCTION),
      Token.new(TokenType::IDENTIFIER, "add"),
      default_token(TokenType::LPAREN),
      Token.new(TokenType::IDENTIFIER, "x"),
      default_token(TokenType::COMMA),
      Token.new(TokenType::IDENTIFIER, "y"),
      default_token(TokenType::RPAREN),
      default_token(TokenType::LBRACE),
      Token.new(TokenType::IDENTIFIER, "x"),
      default_token(TokenType::PLUS),
      Token.new(TokenType::IDENTIFIER, "y"),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::RBRACE),
      default_token(TokenType::SEMICOLON),

      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "result"),
      default_token(TokenType::ASSIGN),
      Token.new(TokenType::IDENTIFIER, "add"),
      default_token(TokenType::LPAREN),
      Token.new(TokenType::IDENTIFIER, "five"),
      default_token(TokenType::COMMA),
      Token.new(TokenType::IDENTIFIER, "ten"),
      default_token(TokenType::RPAREN),
      default_token(TokenType::SEMICOLON),

      default_token(TokenType::IF),
      default_token(TokenType::LPAREN),
      Token.new(TokenType::IDENTIFIER, "result"),
      default_token(TokenType::GREATER_EQUAL),
      Token.new(TokenType::INTEGER, "0"),
      default_token(TokenType::RPAREN),
      default_token(TokenType::LBRACE),
      default_token(TokenType::RETURN),
      default_token(TokenType::TRUE),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::RBRACE),
      default_token(TokenType::ELSE),
      default_token(TokenType::LBRACE),
      default_token(TokenType::RETURN),
      Token.new(TokenType::IDENTIFIER, "result"),
      default_token(TokenType::NOT_EQUAL),
      Token.new(TokenType::INTEGER, "50"),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::RBRACE),

      default_token(TokenType::RETURN),
      default_token(TokenType::FALSE),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::EOF),
    ]

    assert_tokens(Lexer.new(input), ref)
  end

  it "#nextToken comparison" do
    input = "1 < 10 > 5; 1 <= 2 >= 0; 1 == 1; 1 != 2;"
    ref = [
      Token.new(TokenType::INTEGER, "1"),
      default_token(TokenType::LESS),
      Token.new(TokenType::INTEGER, "10"),
      default_token(TokenType::GREATER),
      Token.new(TokenType::INTEGER, "5"),
      default_token(TokenType::SEMICOLON),

      Token.new(TokenType::INTEGER, "1"),
      default_token(TokenType::LESS_EQUAL),
      Token.new(TokenType::INTEGER, "2"),
      default_token(TokenType::GREATER_EQUAL),
      Token.new(TokenType::INTEGER, "0"),
      default_token(TokenType::SEMICOLON),

      Token.new(TokenType::INTEGER, "1"),
      default_token(TokenType::EQUAL),
      Token.new(TokenType::INTEGER, "1"),
      default_token(TokenType::SEMICOLON),

      Token.new(TokenType::INTEGER, "1"),
      default_token(TokenType::NOT_EQUAL),
      Token.new(TokenType::INTEGER, "2"),
      default_token(TokenType::SEMICOLON),

      default_token(TokenType::EOF),
    ]
    assert_tokens(Lexer.new(input), ref)
  end

  it "#nextToken Let Integer" do
    input = "let x = 5;"
    ref = [
      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "x"),
      default_token(TokenType::ASSIGN),
      Token.new(TokenType::INTEGER, "5"),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::EOF),
    ]
    assert_tokens(Lexer.new(input), ref)
  end

  it "#nextToken Let String" do
    input = %(let x = "String";)
    ref = [
      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "x"),
      default_token(TokenType::ASSIGN),
      Token.new(TokenType::STRING, %("String")),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::EOF),
    ]
    assert_tokens(Lexer.new(input), ref)
  end

  it "#nextToken Let Array" do
    input = %(let x = [1, "hello", true];)
    ref = [
      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "x"),
      default_token(TokenType::ASSIGN),
      default_token(TokenType::LBRACKET),
      Token.new(TokenType::INTEGER, "1"),
      default_token(TokenType::COMMA),
      Token.new(TokenType::STRING, %("hello")),
      default_token(TokenType::COMMA),
      default_token(TokenType::TRUE),
      default_token(TokenType::RBRACKET),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::EOF),
    ]
    assert_tokens(Lexer.new(input), ref)
  end

  it "#nextToken Let Hash" do
    input = %(let x = { 1: "one", true: "true", "string": "string" };)
    ref = [
      default_token(TokenType::LET),
      Token.new(TokenType::IDENTIFIER, "x"),
      default_token(TokenType::ASSIGN),
      default_token(TokenType::LBRACE),
      Token.new(TokenType::INTEGER, "1"),
      default_token(TokenType::COLON),
      Token.new(TokenType::STRING, %("one")),
      default_token(TokenType::COMMA),
      default_token(TokenType::TRUE),
      default_token(TokenType::COLON),
      Token.new(TokenType::STRING, %("true")),
      default_token(TokenType::COMMA),
      Token.new(TokenType::STRING, %("string")),
      default_token(TokenType::COLON),
      Token.new(TokenType::STRING, %("string")),
      default_token(TokenType::RBRACE),
      default_token(TokenType::SEMICOLON),
      default_token(TokenType::EOF),
    ]
    assert_tokens(Lexer.new(input), ref)
  end
end
