require "spec"
require "../src/capuchin.cr"

TOKEN_MAPPING = {
  TokenType::ASSIGN        => "=",
  TokenType::PLUS          => "+",
  TokenType::MINUS         => "-",
  TokenType::ASTERISK      => "*",
  TokenType::SLASH         => "/",
  TokenType::BANG          => "!",
  TokenType::EQUAL         => "==",
  TokenType::NOT_EQUAL     => "!=",
  TokenType::LESS          => "<",
  TokenType::LESS_EQUAL    => "<=",
  TokenType::GREATER       => ">",
  TokenType::GREATER_EQUAL => ">=",
  TokenType::COMMA         => ",",
  TokenType::COLON         => ":",
  TokenType::SEMICOLON     => ";",
  TokenType::LPAREN        => "(",
  TokenType::RPAREN        => ")",
  TokenType::LBRACKET      => "[",
  TokenType::RBRACKET      => "]",
  TokenType::LBRACE        => "{",
  TokenType::RBRACE        => "}",
  TokenType::FUNCTION      => "fun",
  TokenType::LET           => "let",
  TokenType::IF            => "if",
  TokenType::ELSE          => "else",
  TokenType::RETURN        => "return",
  TokenType::TRUE          => "true",
  TokenType::FALSE         => "false",
  TokenType::EOF           => Char::ZERO.to_s,
}

macro default_token(tokentype)
  Token.new({{tokentype}}, TOKEN_MAPPING[{{tokentype}}])
end

def assert_tokens(lexer : Capuchin::Lexer, refs : Array(Capuchin::Token))
  idx = 0
  while idx < refs.size
    token = lexer.nextToken
    ref = refs[idx]
    {token.ttype, token.literal}.should(eq({ref.ttype, ref.literal}))
    idx += 1
  end
end

macro test_eval(input)
  interpreter = Interpreter.new(Environment.new)
  # load builtins
  interpreter.eval(
    Capuchin::Parser.new(Capuchin::Lexer.new(Capuchin::BuiltinsSource)).parse)
  interpreter.eval(Parser.new(Lexer.new({{input}})).parse)
end
