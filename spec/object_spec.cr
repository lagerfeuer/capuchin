require "./spec_helper"
require "../src/capuchin/eval/object.cr"

include Capuchin

describe "Object" do
  it "#hash_key" do
    str1 = CString.new("HelloWorld")
    str2 = CString.new("hello_world".camelcase)

    str1.value.should(eq(str2.value))
    str1.value.object_id.should_not(eq(str2.value.object_id))
    str1.hash_key.should(eq(str2.hash_key))

    int1 = Integer.new(111)
    int2 = Integer.new(111)
    int1.value.should(eq(int2.value))
    int1.hash_key.should(eq(int2.hash_key))
  end
end
