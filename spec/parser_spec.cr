require "./spec_helper"
require "../src/capuchin/parser/parser.cr"
require "../src/capuchin/token/token.cr"

include Capuchin

macro parse(program)
  Parser.new(Lexer.new({{program}})).parse
end

describe Parser do
  it "#parseLetStatement raises when no expression" do
    expect_raises(ParserError) do
      Parser.new(Lexer.new("let x = ;")).parse
    end
  end
  it "#parseLetStatement raises when no ASSIGN token" do
    expect_raises(ParserError) do
      Parser.new(Lexer.new("let x + 5;")).parse
    end
  end
  it "#parseLetStatement raises when no valid identifier" do
    expect_raises(ParserError) do
      Parser.new(Lexer.new("let 1 = 5;")).parse
    end
  end

  it "LetStatement missing semicolon" do
    expect_raises(ParserError) do
      Parser.new(Lexer.new("let x = 5")).parse
    end
  end
  it "ReturnStatement missing semicolon" do
    expect_raises(ParserError) do
      Parser.new(Lexer.new("return 3")).parse
    end
  end
  it "ExpressionStatement missing semicolon" do
    expect_raises(ParserError) do
      Parser.new(Lexer.new("5 + 5")).parse
    end
  end

  it "#parse Boolean" do
    ast = Parser.new(Lexer.new("true;\nfalse;")).parse
    ast.statements.zip([true, false]).each do |node, ref|
      expr = node.as(AST::ExpressionStatement).expr
      expr.should(be_a(AST::Boolean))
      expr.as(AST::Boolean).value.should(eq(ref))
    end
  end

  it "#parse Integer" do
    ast = Parser.new(Lexer.new("2;\t3;")).parse
    ast.statements.zip([2_i64, 3_i64]).each do |node, ref|
      expr = node.as(AST::ExpressionStatement).expr
      expr.should(be_a(AST::Integer))
      expr.as(AST::Integer).value.should(eq(ref))
    end
  end

  it "#parse Identifier" do
    ast = Parser.new(Lexer.new("var; var2;")).parse
    ast.statements.zip(%w[var var2]).each do |node, ref|
      expr = node.as(AST::ExpressionStatement).expr
      expr.should(be_a(AST::Identifier))
      expr.as(AST::Identifier).value.should(eq(ref))
    end
  end

  it "#parse String" do
    ast = Parser.new(Lexer.new(%("string";\t"and_again";))).parse
    ast.statements.zip(%w[string and_again]).each do |node, ref|
      expr = node.as(AST::ExpressionStatement).expr
      expr.should(be_a(AST::CString))
      expr.as(AST::CString).value.should(eq(ref))
    end
  end

  it "#parse Array" do
    ast = Parser.new(Lexer.new(%([1, "hello", true];))).parse
    ast.statements.size.should(eq(1))
    expr = ast.statements.first.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CArray))
    expr.as(AST::CArray).elements.size.should(eq(3))
  end

  it "#parse Hash" do
    ast = Parser.new(Lexer.new(%({ 1: "number", "hello": "string", true: "bool" };))).parse
    ast.statements.size.should(eq(1))
    expr = ast.statements.first.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CHash))
    expr.as(AST::CHash).elements.size.should(eq(3))
    hash = expr.as(AST::CHash).elements
    refs = {"1" => "number", "hello" => "string", "true" => "bool"}
    hash.map { |k, v| {k.to_s, v.to_s} }.zip(refs) do |elem, ref|
      elem.should(eq(ref))
    end
  end

  it "#parse empty Hash" do
    ast = Parser.new(Lexer.new("{};")).parse
    ast.statements.size.should(eq(1))
    expr = ast.statements.first.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CHash))
    expr.as(AST::CHash).elements.size.should(eq(0))
  end

  it "#parse Hash with Expressions" do
    ast = Parser.new(Lexer.new(%({ "one": 0 + 1, "two": 10 - 8, "three": 15 / 5 };))).parse
    ast.statements.size.should(eq(1))
    expr = ast.statements.first.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CHash))
    expr.as(AST::CHash).elements.size.should(eq(3))
    hash = expr.as(AST::CHash).elements
    refs = {"one" => "0 + 1", "two" => "10 - 8", "three" => "15 / 5"}
    hash.map { |k, v| {k.to_s, v.to_s} }.zip(refs) do |elem, ref|
      elem.should(eq(ref))
    end
  end

  it "#parse IndexExpression (Array Access)" do
    ast = Parser.new(Lexer.new("myArray[1+1];")).parse
    expr = ast.statements.first.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::IndexExpression))
    expr.as(AST::IndexExpression).expr.should(be_a(AST::Identifier))
    expr.as(AST::IndexExpression).index.should(be_a(AST::InfixExpression))
  end

  it "#parse LetStatement" do
    program = <<-EOS
let x = 5;
let y = abc;
let z = -3;
let bool = !!true;
EOS
    parser = Parser.new(Lexer.new(program))
    ast = parser.parse
    lines = ast.statements.map(&.source_code)
    refs = program.split("\n")
    lines.should(eq(refs))
  end

  it "#parse ReturnStatement" do
    program = <<-EOS
return;
return 5;
return a;
EOS
    parser = Parser.new(Lexer.new(program))
    ast = parser.parse
    lines = ast.statements.map(&.source_code)
    refs = program.split("\n")
    lines.should(eq(refs))
  end

  it "#parse Identifier Expression" do
    program = "identifier;"
    ast = Parser.new(Lexer.new(program)).parse
    ast.statements.size.should(eq(1))
    ast.statements.first.should(be_a(AST::ExpressionStatement))
    expr_stmt = ast.statements.first.as(AST::ExpressionStatement)
    expr_stmt.expr.should(be_a(AST::Identifier))
    id = expr_stmt.expr.as(AST::Identifier)
    id.value.should(eq("identifier"))
  end

  it "#parse Integer Expression" do
    program = "1337;"
    ast = Parser.new(Lexer.new(program)).parse
    ast.statements.size.should(eq(1))
    ast.statements.first.should(be_a(AST::ExpressionStatement))
    expr_stmt = ast.statements.first.as(AST::ExpressionStatement)
    expr_stmt.expr.should(be_a(AST::Integer))
    int = expr_stmt.expr.as(AST::Integer)
    int.value.should(eq(1337))
  end

  it "#parse PrefixExpressions" do
    program = <<-EOS
!5;
-15;
EOS
    ast = Parser.new(Lexer.new(program)).parse
    ast.statements.size.should(eq(2))
    ast.statements.each do |stmt|
      stmt.should(be_a(AST::ExpressionStatement))
    end
    exprs = ast.statements.map do |stmt|
      stmt.as(AST::ExpressionStatement).expr.as(AST::PrefixExpression)
    end

    outpairs = exprs.map do |expr|
      {expr.operator.literal, expr.expr.as(AST::Integer).value}
    end
    refs = [{"!", 5}, {"-", 15}]
    outpairs.zip(refs).each do |out, ref|
      out.should(eq(ref))
    end
  end
  it "#parse InfixExpressions" do
    program = <<-EOS
a + 1;
a - 1;
a * 1;
a / 1;
a < 1;
a > 1;
a == 1;
a != 1;
EOS
    ast = Parser.new(Lexer.new(program)).parse
    ast.statements.size.should(eq(8))
    ast.statements.each do |stmt|
      stmt.should(be_a(AST::ExpressionStatement))
    end

    outs = ast.statements.map do |stmt|
      expr = stmt.as(AST::ExpressionStatement).expr.as(AST::InfixExpression)
      {expr.left.as(AST::Identifier).value,
       expr.operator.literal,
       expr.right.as(AST::Integer).value}
    end
    refs = %w[+ - * / < > == !=].map { |op| {"a", op, 1} }

    outs.zip(refs).each do |out, ref|
      out.should(eq(ref))
    end
  end

  it "#parses nested Expressions" do
    ast = Parser.new(Lexer.new("1 + 2 * 3 == 4 * 5 + 6;")).parse
    expr = ast.statements.shift.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::InfixExpression))
    expr.as(AST::InfixExpression).operator.literal.should(eq("=="))
    left = expr.as(AST::InfixExpression).left
    left.should(be_a(AST::InfixExpression))
    left.as(AST::InfixExpression).operator.literal.should(eq("+"))
    left.as(AST::InfixExpression).right.should(be_a(AST::InfixExpression))
    left_inner = left.as(AST::InfixExpression).right
    left_inner.as(AST::InfixExpression).operator.literal.should(eq("*"))
    right = expr.as(AST::InfixExpression).right
    right.should(be_a(AST::InfixExpression))
    right.as(AST::InfixExpression).operator.literal.should(eq("+"))
    right.as(AST::InfixExpression).left.should(be_a(AST::InfixExpression))
    right_inner = right.as(AST::InfixExpression).left
    right_inner.as(AST::InfixExpression).operator.literal.should(eq("*"))
  end

  it "#parse nested Prefix and Infix Expressions mixed together" do
    ast = Parser.new(Lexer.new("-a * b;")).parse
    expr = ast.statements.shift.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::InfixExpression))
    expr.as(AST::InfixExpression).left.should(be_a(AST::PrefixExpression))
  end

  it "#parse nested Infix Expressions" do
    ast = Parser.new(Lexer.new("!-a;")).parse
    expr = ast.statements.shift.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::PrefixExpression))
    expr.as(AST::PrefixExpression).expr.should(be_a(AST::PrefixExpression))
  end

  it "#parse nested Infix Expressions (same precedence)" do
    ast = Parser.new(Lexer.new("a + b + c;")).parse
    expr = ast.statements.shift.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::InfixExpression))
    expr.as(AST::InfixExpression).left.should(be_a(AST::InfixExpression))
  end

  it "#parses nested Expressions (with parenthesis)" do
    ast = Parser.new(Lexer.new("(1 + 2) * 3 == 4 * (5 + 6);")).parse
    expr = ast.statements.shift.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::InfixExpression))
    expr.as(AST::InfixExpression).operator.literal.should(eq("=="))
    left = expr.as(AST::InfixExpression).left
    left.should(be_a(AST::InfixExpression))
    left.as(AST::InfixExpression).operator.literal.should(eq("*"))
    left.as(AST::InfixExpression).left.should(be_a(AST::InfixExpression))
    left_inner = left.as(AST::InfixExpression).left
    left_inner.as(AST::InfixExpression).operator.literal.should(eq("+"))
    right = expr.as(AST::InfixExpression).right
    right.should(be_a(AST::InfixExpression))
    right.as(AST::InfixExpression).operator.literal.should(eq("*"))
    right.as(AST::InfixExpression).right.should(be_a(AST::InfixExpression))
    right_inner = right.as(AST::InfixExpression).right
    right_inner.as(AST::InfixExpression).operator.literal.should(eq("+"))
  end

  it "#parse if" do
    program = <<-EOS
if (a > b) {
  return x;
}
EOS
    ast = Parser.new(Lexer.new(program)).parse
    stmt = ast.statements.shift
    stmt.should(be_a(AST::ExpressionStatement))
    stmt = stmt.as(AST::ExpressionStatement)
    stmt.expr.should(be_a(AST::IfExpression))
    expr = stmt.expr.as(AST::IfExpression)
    expr.condition.should(be_a(AST::InfixExpression))
    expr.trueStmt.should(be_a(AST::CompoundStatement))
    expr.falseStmt.should(be_nil)
  end

  it "#parse if else" do
    program = <<-EOS
if (a > b) {
  return x;
} else {
  return y;
}
EOS
    ast = Parser.new(Lexer.new(program)).parse
    stmt = ast.statements.shift
    stmt.should(be_a(AST::ExpressionStatement))
    stmt = stmt.as(AST::ExpressionStatement)
    stmt.expr.should(be_a(AST::IfExpression))
    expr = stmt.expr.as(AST::IfExpression)
    expr.condition.should(be_a(AST::InfixExpression))
    expr.trueStmt.should(be_a(AST::CompoundStatement))
    expr.falseStmt.should(be_a(AST::CompoundStatement))
  end

  # NOT WORKING ANYMORE (since Hash was added)
  #   it "#parse CompoundStatement" do
  #     program = <<-EOS
  # {
  #   let a = 1;
  #   return a + 1;
  # }
  # EOS
  #     ast = Parser.new(Lexer.new(program)).parse
  #     ast.statements.size.should(eq(1))
  #     stmt = ast.statements.shift.as(AST::CompoundStatement)
  #     stmt.stmts.size.should(eq(2))
  #     stmt.stmts.shift.should(be_a(AST::LetStatement))
  #     stmt.stmts.shift.should(be_a(AST::ReturnStatement))
  #   end

  it "#parse FunctionExpressions" do
    program = <<-EOS
fun() {
  let a = 1;
  return a;
}

fun(a) {
  let b = false;
  return a == b;
}

fun(a, b, c) { let z = c + 1; return a + b + z; }
EOS
    ast = parse(program)
    funs = ast.statements.map(&.as(AST::ExpressionStatement).expr)
    funs.each do |f|
      f.should(be_a(AST::Function))
    end
    funs = funs.map(&.as(AST::Function))
    funs.zip([[] of String, %w[a], %w[a b c]]).each do |f, args_str|
      f.parameters.map(&.value).should(eq(args_str))
    end

    funs.each do |f|
      f.body.stmts.first.should(be_a(AST::LetStatement))
      f.body.stmts.last.should(be_a(AST::ReturnStatement))
    end
  end

  it "#parse CallExpressions Identifier" do
    program = <<-EOS
sum(1, 2 * 3, 4 + 5);
void();
EOS
    ast = parse(program)

    stmt = ast.statements.shift
    stmt.should(be_a(AST::ExpressionStatement))
    expr = stmt.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CallExpression))
    expr = expr.as(AST::CallExpression)
    expr.function.should(be_a(AST::Identifier))
    args = expr.arguments
    args.shift.should(be_a(AST::Integer))
    args.shift.should(be_a(AST::InfixExpression))
    args.shift.should(be_a(AST::InfixExpression))

    stmt = ast.statements.shift
    stmt.should(be_a(AST::ExpressionStatement))
    expr = stmt.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CallExpression))
    expr = expr.as(AST::CallExpression)
    expr.function.should(be_a(AST::Identifier))
    expr.arguments.should(be_empty)
  end

  it "#parse CallExpressions Function" do
    program = <<-EOS
fun (a, b) { return a + b; }(1, 2);
fun () { let a = 3; }();
EOS
    ast = parse(program)

    stmt = ast.statements.shift
    stmt.should(be_a(AST::ExpressionStatement))
    expr = stmt.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CallExpression))
    expr = expr.as(AST::CallExpression)
    expr.function.should(be_a(AST::Function))
    expr.arguments.size.should(eq(2))

    stmt = ast.statements.shift
    stmt.should(be_a(AST::ExpressionStatement))
    expr = stmt.as(AST::ExpressionStatement).expr
    expr.should(be_a(AST::CallExpression))
    expr = expr.as(AST::CallExpression)
    expr.function.should(be_a(AST::Function))
    expr.arguments.should(be_empty)
  end

  it "#parse CallExpressions Precedence" do
    program1 = <<-EOS
a + sum(b * c) + d;
add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8));
add(a + b + c * d / e * f + g);
EOS
    program2 = <<-EOS
((a + sum((b * c)) + d));
add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)));
add(((a + b) + ((c * d) / e)) * f + g);
EOS

    ast1 = parse(program1)
    ast2 = parse(program2)
    # TODO implement proper comparison
    ast1.statements.zip(ast2.statements).each do |stmt1, stmt2|
      stmt1.source_code.should(eq(stmt2.source_code))
    end
  end

  it "#parse GroupedExpressions Tokens Correctness" do
    expr1 = parse("a + b;").statements.first.source_code
    expr2 = parse("(a + b);").statements.first.source_code
    expr3 = parse("((a) + b);").statements.first.source_code

    expr1.should(eq(expr2))
    expr1.should(eq(expr3))
  end
end
