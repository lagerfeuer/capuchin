require "./spec_helper"
require "../src/capuchin/eval/eval.cr"

include Capuchin

describe "Eval" do
  it "#eval Unknown Identifier" do
    expect_raises(EvalError) do
      test_eval("abcdefghijkl();")
    end
  end

  it "#eval Integer" do
    int = test_eval("5;")
    int.should(be_a(Integer))
    int.as(Integer).value.should(eq(5_i64))
  end

  it "#eval Boolean" do
    int = test_eval("true;")
    int.should(be_a(Boolean))
    int.as(Boolean).value.should(eq(true))
    int = test_eval("false;")
    int.should(be_a(Boolean))
    int.as(Boolean).value.should(eq(false))
  end

  it "#eval String" do
    str = test_eval(%("string";))
    str.should(be_a(CString))
    str.as(CString).value.should(eq(%(string)))
  end

  it "#eval String concat" do
    str = test_eval(%("Hello " + "World!";))
    str.should(be_a(CString))
    str.as(CString).value.should(eq(%(Hello World!)))
  end

  it "#eval Array" do
    array = test_eval(%([1,2,3,4];))
    array.should(be_a(CArray))
    array.as(CArray).elements.size.should(eq(4))
    array.as(CArray).elements.each_with_index { |e, i| e.value.should(eq((i + 1).to_i64)) }
  end

  it "#eval Array Access" do
    input = <<-EOS
[1,2,3,4][0];
[1,2,3,4][1];
[1,2,3,4][2];
[1,2,3,4][3];
let x = [1,2,3,4]; x[0];
EOS
    refs = [1, 2, 3, 4, 1]
    input.lines.zip(refs) do |line, ref|
      int = test_eval(line)
      int.should(be_a(Integer))
      int.as(Integer).value.should(eq(ref))
    end
  end

  it "#eval Hash" do
    hash = test_eval(<<-EOS
let two = "two";
{
  "one": 10 - 9,
  two: 1 + 1,
  "thr" + "ee": 6 / 2,
  4: 4,
  true: 5,
  false: 6
};
EOS
    )
    hash.should(be_a(CHash))
    hash = hash.as(CHash)
    hash.elements.size.should(eq(6))
    hash.elements.values.each_with_index do |e, i|
      e.value.value.should(eq(i + 1))
    end
  end

  it "#eval Hash Access" do
    input = <<-EOS
{"five": 5}["five"];
let key = "five"; {"five": 5}[key];
{5: 5}[5];
{true: 5}[true];
{false: 5}[false];
EOS
    input.lines.each do |line|
      val = test_eval(line)
      val.should(be_a(Integer))
      val.as(Integer).value.should(eq(5))
    end
  end

  it "#eval Hash Access Null" do
    input = <<-EOS
{"five": 5}["six"];
{}["key"];
EOS
    input.lines.each do |line|
      val = test_eval(line)
      val.should(be_a(Null))
    end
  end

  it "#eval prefix:bang" do
    input = <<-EOS
!true;
!false;
!5;
!!true;
!!false;
!!5;
!0;
!!0;
EOS
    refs = [false, true, false, true, false, true, true, false]
    input.lines.zip(refs) do |line, ref|
      bool = test_eval(line)
      bool.should(be_a(Boolean))
      bool.as(Boolean).value.should(eq(ref))
    end
  end

  it "#eval prefix:minus" do
    input = <<-EOS
-5;
-10;
-0;
EOS
    refs = [-5, -10, 0].map(&.to_i64)
    input.lines.zip(refs) do |line, ref|
      int = test_eval(line)
      int.should(be_a(Integer))
      int.as(Integer).value.should(eq(ref))
    end
  end

  it "#eval Integer infix" do
    input = <<-EOS
5 + 5 + 5 + 5 - 10;
2 * 2 * 2 * 2 * 2;
-50 + 100 + -50;
5 * 2 + 10;
5 + 2 * 10;
20 + 2 * -10;
50 / 2 * 2 + 10;
2 * (5 + 10);
3 * 3 * 3 + 10;
3 * (3 * 3) + 10;
(5 + 10 * 2 +15 / 3) * 2 + -10;
EOS
    refs = [10, 32, 0, 20, 25, 0, 60, 30, 37, 37, 50]
    input.lines.zip(refs) do |line, ref|
      int = test_eval(line)
      int.should(be_a(Integer))
      int.as(Integer).value.should(eq(ref))
    end
  end

  it "#eval Integer comparison" do
    input = <<-EOS
1 < 2;
1 > 2;
2 < 1;
2 > 1;
1 == 2;
1 == 1;
1 != 2;
1 != 1;
1 <= 2;
1 >= 2;
3 <= 2;
3 >= 2;
EOS
    refs = [true, false, false, true,
            false, true, true, false,
            true, false, false, true]
    input.lines.zip(refs) do |line, ref|
      int = test_eval(line)
      int.should(be_a(Boolean))
      int.as(Boolean).value.should(eq(ref))
    end
  end

  it "#eval Boolean comparison" do
    input = <<-EOS
(1 < 2) == true;
(1 > 2) == false;
(2 < 1) != true;
(2 > 1) != false;
true == true;
false == false;
true == false;
true != true;
false != false;
true != false;
EOS
    refs = [true, true, true, true,
            true, true, false,
            false, false, true]
    input.lines.zip(refs) do |line, ref|
      int = test_eval(line)
      int.should(be_a(Boolean))
      int.as(Boolean).value.should(eq(ref))
    end
  end

  it "#eval If expression" do
    input = <<-EOS
if (true) { 10; }
if (false) { 10; }
if (1) { 10; }
if (0) { 10; }
if (1 < 2) { 10; }
if (1 > 2) { 10; }
if (1 < 2) { 10; } else { 20; }
if (1 > 2) { 10; } else { 20; }
EOS
    ten = Integer.new(10)
    refs = [
      ten, nil, ten, nil,
      ten, nil, ten, Integer.new(20),
    ]
    input.lines.zip(refs) do |line, ref|
      result = test_eval(line)
      if ref.nil?
        result.should(be_a(Null))
      else
        result.should(be_a(Integer))
        result.as(Integer).value.should(eq(ref.value))
      end
    end
  end

  it "#eval Return Statement" do
    input = <<-EOS
return 10;
return 10; 9;
return 2 * 5; 9;
9; return 2*5; 9;
EOS
    input.lines.each do |line|
      result = test_eval(line)
      result.should(be_a(Integer))
      result.as(Integer).value.should(eq(10))
    end
  end

  it "#eval Errors" do
    input = <<-EOS
5 + true;
5 + true; 5;
-true;
true + false;
5; true + false; 5;
if (10 > 1) { true + false; }
if (10 > 1) { if (10 > 1) { return true + false; } return 1; }
EOS

    input.lines.each do |line|
      result = test_eval(line)
      result.should(be_a(Error))
    end
  end

  it "#eval environment" do
    input = <<-EOS
let a = 5; a;
let a = 5 * 5; a;
let a = 5; let b = a; b;
let a = 5; let b = a; let c = a + b + 5; c;
EOS
    refs = [5, 25, 5, 15]
    input.lines.zip(refs).each do |line, ref|
      result = test_eval(line)
      result.should(be_a(Integer))
      result.as(Integer).value.should(eq(ref))
    end
  end

  it "#eval accepts Functions" do
    input = "fun(x) { x * 3; }"
    result = test_eval(input)
    result.should(be_a(Function))
    func = result.as(Function)
    func.parameters.size.should(eq(1))
    func.body.stmts.size.should(eq(1))
    expr = func.body.stmts.shift
    expr.to_s.should(eq("x * 3"))
  end

  it "#eval interprets Functions" do
    input = <<-EOS
let id = fun (x) { x; }; id(5);
let id = fun (x) { return x; }; id(5);
let double = fun (x) { x * 2; }; double(5);
let add = fun (x, y) { x + y; }; add(5, 5);
let add = fun (x, y) { x + y; }; add(5 + 5, add(5, 5));
fun (x) { x; }(5);
EOS
    refs = [5, 5, 10, 10, 20, 5]
    input.lines.zip(refs).each do |line, ref|
      result = test_eval(line)
      result.should(be_a(Integer))
      result.as(Integer).value.should(eq(ref))
    end
  end

  it "#eval Function Closure" do
    input = <<-EOS
let newAdder = fun(x) {
  return fun(y) {
    return x + y;
  };
};
let addTwo = newAdder(2);
addTwo(2);
EOS
    result = test_eval(input)
    result.should(be_a(Integer))
    result.as(Integer).value.should(eq(4))
  end

  it "#eval Builtin len" do
    int = test_eval(%[len("");])
    int.should(be_a(Integer))
    int.as(Integer).value.should(eq(0))

    int = test_eval(%[len("Hello");])
    int.should(be_a(Integer))
    int.as(Integer).value.should(eq(5))

    int = test_eval(%(len([]);))
    int.should(be_a(Integer))
    int.as(Integer).value.should(eq(0))

    int = test_eval(%(len([1,2,3,4]);))
    int.should(be_a(Integer))
    int.as(Integer).value.should(eq(4))
  end

  it "#eval Builtin first" do
    result = test_eval(%(first([]);))
    result.should(be_a(Null))

    result = test_eval(%(first([1,2,3,4]);))
    result.should(be_a(Integer))
    result.as(Integer).value.should(eq(1))
  end

  it "#eval Builtin last" do
    result = test_eval(%(last([]);))
    result.should(be_a(Null))

    result = test_eval(%(last([1,2,3,4]);))
    result.should(be_a(Integer))
    result.as(Integer).value.should(eq(4))
  end

  it "#eval Builtin rest" do
    result = test_eval(%(rest([]);))
    result.should(be_a(Null))

    result = test_eval(%(rest([1,2,3,4]);))
    result.should(be_a(CArray))
    result.as(CArray).elements.zip([2, 3, 4]) do |elem, ref|
      elem.as(Integer).value.should(eq(ref))
    end
  end
end
