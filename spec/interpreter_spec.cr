require "./spec_helper"
require "../src/capuchin/eval/eval.cr"

include Capuchin

describe "Functionality" do
  it "#map" do
    prog = <<-EOS
let arr = [1,2,3,4];
let double = fun(x) { return x * 2; };
map(arr, double);
EOS
    refs = [2, 4, 6, 8]
    result = test_eval(prog)
    result.should(be_a(CArray))
    result.as(CArray).elements.zip(refs).each do |elem, ref|
      elem.as(Integer).value.should(eq(ref))
    end
  end

  it "#reduce" do
    prog = <<-EOS
let arr = ["a", "b", "c"];
let concat = fun(a,b) { return a + b; };
reduce(arr, "", concat);
EOS
    result = test_eval(prog)
    result.should(be_a(CString))
    result.as(CString).value.should(eq("abc"))
  end

  it "#sum" do
    prog = <<-EOS
let arr = [1,2,3,4];
sum(arr);
EOS
    result = test_eval(prog)
    result.should(be_a(Integer))
    result.as(Integer).value.should(eq(10))
  end
end
