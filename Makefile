CC := crystal
BIN := out/capuchin
MAIN := src/main.cr
# SRC := $(shell find src/ -not -path "*/\.*" -type f -name "*.cr")

all: build

$(BIN): $(MAIN)
	@mkdir -p $(dir $(BIN))
	$(CC) build $(MAIN) $(FLAGS) -o $(BIN)

build: $(BIN)

release:
	@$(MAKE) -B --no-print-directory FLAGS="--release --no-debug" build

run:
	@$(CC) run $(MAIN)

test:
	$(CC) spec

docs:
	$(CC) docs

clean:
	@rm -rf bin

.PHONY: all build release run test docs clean
