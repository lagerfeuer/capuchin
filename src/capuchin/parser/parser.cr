require "../lexer/lexer.cr"
require "../token/*"
require "../ast/*"
require "../errors.cr"

module Capuchin
  enum Precedence : Int8
    LOWEST      = 1
    EQUALS
    LESSGREATER
    SUM
    PRODUCT
    PREFIX
    CALL
    INDEX
  end

  PRECEDENCE_MAP = {
    TokenType::EQUAL         => Precedence::EQUALS,
    TokenType::NOT_EQUAL     => Precedence::EQUALS,
    TokenType::LESS          => Precedence::LESSGREATER,
    TokenType::LESS_EQUAL    => Precedence::LESSGREATER,
    TokenType::GREATER       => Precedence::LESSGREATER,
    TokenType::GREATER_EQUAL => Precedence::LESSGREATER,
    TokenType::PLUS          => Precedence::SUM,
    TokenType::MINUS         => Precedence::SUM,
    TokenType::ASTERISK      => Precedence::PRODUCT,
    TokenType::SLASH         => Precedence::PRODUCT,
    TokenType::LPAREN        => Precedence::CALL,
    TokenType::LBRACKET      => Precedence::INDEX,
  }

  alias PrefixParseProc = Proc(AST::Identifier) |
                          Proc(AST::Integer) |
                          Proc(AST::Boolean) |
                          Proc(AST::CString) |
                          Proc(AST::CArray) |
                          Proc(AST::CHash) |
                          Proc(AST::Function) |
                          Proc(AST::PrefixExpression) |
                          Proc(AST::IfExpression) |
                          Proc(AST::Expression)
  alias InfixParseProc = Proc(AST::Expression, AST::Identifier) |
                         Proc(AST::Expression, AST::Integer) |
                         Proc(AST::Expression, AST::Boolean) |
                         Proc(AST::Expression, AST::CString) |
                         Proc(AST::Expression, AST::PrefixExpression) |
                         Proc(AST::Expression, AST::InfixExpression) |
                         Proc(AST::Expression, AST::IfExpression) |
                         Proc(AST::Expression, AST::CallExpression) |
                         Proc(AST::Expression, AST::IndexExpression) |
                         Proc(AST::Expression, AST::Expression)

  # Parser class
  class Parser
    property lexer
    @current : Token
    @next : Token

    # TODO maybe call `Lexer::getAll` or create `Lexer#getTokens`
    # and simply save `Array(Token)` and use `@pos` as pointer
    # to the tokens
    def initialize(@lexer : Lexer)
      # tokens
      @current = Token.new(TokenType::INVALID, nil)
      @next = Token.new(TokenType::INVALID, nil)
      # parser functions
      @prefixParsers = {} of TokenType => PrefixParseProc
      @infixParsers = {} of TokenType => InfixParseProc
      infixParser = ->parseInfixExpression(AST::Expression)

      registerPrefix(TokenType::IDENTIFIER, ->parseIdentifier)
      registerPrefix(TokenType::INTEGER, ->parseInteger)
      registerPrefix(TokenType::STRING, ->parseString)
      registerPrefix(TokenType::LBRACKET, ->parseArray)
      registerPrefix(TokenType::LBRACE, ->parseHash)
      registerPrefix(TokenType::TRUE, ->parseBoolean)
      registerPrefix(TokenType::FALSE, ->parseBoolean)
      registerPrefix(TokenType::FUNCTION, ->parseFunction)
      registerPrefix(TokenType::BANG, ->parsePrefixExpression)
      registerPrefix(TokenType::MINUS, ->parsePrefixExpression)
      registerPrefix(TokenType::LPAREN, ->parseGroupedExpression)
      registerPrefix(TokenType::IF, ->parseIfExpression)

      registerInfix(TokenType::PLUS, infixParser)
      registerInfix(TokenType::MINUS, infixParser)
      registerInfix(TokenType::ASTERISK, infixParser)
      registerInfix(TokenType::SLASH, infixParser)
      registerInfix(TokenType::EQUAL, infixParser)
      registerInfix(TokenType::NOT_EQUAL, infixParser)
      registerInfix(TokenType::LESS, infixParser)
      registerInfix(TokenType::LESS_EQUAL, infixParser)
      registerInfix(TokenType::GREATER, infixParser)
      registerInfix(TokenType::GREATER_EQUAL, infixParser)
      registerInfix(TokenType::LPAREN, ->parseCallExpression(AST::Expression))
      registerInfix(TokenType::LBRACKET, ->parseIndexExpression(AST::Expression))

      # initialize both currentt and next token
      nextToken
      nextToken
    end

    # Parses the program input and returns an `AST` object.
    def parse : AST::AST
      program = [] of AST::Statement
      while @current.ttype != TokenType::EOF
        program << parseStatement
        nextToken
      end
      return AST::AST.new(program)
    end

    private def parseStatement : AST::Statement
      # each case must advance the token head `@current` to the
      # semicolon of that statement.
      case @current.ttype
      when TokenType::LET    then stmt = parseLetStatement
      when TokenType::RETURN then stmt = parseReturnStatement
      else                        stmt = parseExpressionStatement
      end
      return stmt
    end

    # Returns nil if *expected* and *@current.ttype* have the same type.
    # Raises otherwise.
    private def expect(expected : TokenType) : Nil
      raise ParserError.new(
        "Expected #{expected}, but got #{@current.ttype}"
      ) if expected != @current.ttype
    end

    # Returns nil if *expected* and *@next.ttype* have the same type.
    # Raises otherwise.
    # NOTE Advances the token head, i.e. calls `Parser#nextToken`
    private def nextTokenExpect(expected : TokenType) : Nil
      nextToken
      expect(expected)
    end

    # Returns nil if one of *expected* and *@current.ttype* have the same type.
    # Raises otherwise.
    private def expect(*expected) : Nil
      raise ParserError.new(
        %(Expected one of #{expected.to_a.join(" or ")}, ) \
        %(but got #{@current.ttype})
      ) unless expected.to_a.includes?(@current.ttype)
    end

    # Returns nil if one of *expected* and *@next.ttype* have the same type.
    # Raises otherwise.
    # NOTE Advances the token head, i.e. calls `Parser#nextToken`
    private def nextTokenExpect(*expected) : Nil
      nextToken
      expect(*expected)
    end

    # Returns whether the *@current* token type matches *ttype*.
    private def currentTokenIs(ttype : TokenType) : Bool
      return @current.ttype === ttype
    end

    # Returns whether the *@next* token type matches *ttype*.
    private def nextTokenIs(ttype : TokenType) : Bool
      return @next.ttype === ttype
    end

    # Returns the next token, given by `Lexer#nextToken`.
    private def nextToken : Token
      @current = @next
      @next = lexer.nextToken
      return @current
    end

    # Returns the precedence of the current token.
    private def currentPrecedence : Precedence
      return PRECEDENCE_MAP.fetch(@current.ttype, Precedence::LOWEST)
    end

    # Returns the precendence of the next token.
    private def nextPrecedence : Precedence
      return PRECEDENCE_MAP.fetch(@next.ttype, Precedence::LOWEST)
    end

    # Parses the current token as `AST::Identifier`.
    # **DOES NOT** advance the token head (i.e. the `@current` token
    # stays the same).
    private def parseIdentifier : AST::Identifier
      return AST::Identifier.new(@current)
    end

    # Parses the current token as `AST::Integer`.
    # NOTE: **DOES NOT** advance the token head (i.e. the `@current` token
    # stays the same).
    private def parseInteger : AST::Integer
      return AST::Integer.new(@current)
    end

    # Parses the current token as `AST::CString`.
    # NOTE: **DOES NOT** advance the token head (i.e. the `@current` token
    # stays the same).
    private def parseString : AST::CString
      raise ParserError.new(
        "Unterminated String Literal: #{@current}"
      ) unless @current.literal.ends_with?('"')

      return AST::CString.new(@current)
    end

    # Parses the following tokens (until ']') as `AST::CArray`.
    # NOTE: **DOES** advance the token head (i.e. the `@current` token
    # will be on the ']' token after this step).
    private def parseArray : AST::CArray
      open = @current
      elements = parseExpressionList(TokenType::RBRACKET)
      close = @current
      return AST::CArray.new(open, elements, close)
    end

    # Parses the following tokens (until '}') as `AST::CHash`.
    # NOTE: **DOES** advance the token head (i.e. the `@current` token
    # will be on the '}' token after this step).
    private def parseHash : AST::CHash
      open = @current
      pairs = {} of AST::Expression => AST::Expression

      nextToken
      while !currentTokenIs(TokenType::RBRACE)
        key = parseExpression(Precedence::LOWEST)
        nextTokenExpect(TokenType::COLON)
        nextToken
        value = parseExpression(Precedence::LOWEST)
        nextToken
        pairs[key] = value

        expect(TokenType::RBRACE, TokenType::COMMA)
        nextToken if @current.ttype == TokenType::COMMA
      end

      close = @current
      return AST::CHash.new(open, pairs, close)
    end

    # Parses the current token as `AST::Boolean`.
    # NOTE: **DOES NOT** advance the token head (i.e. the `@current` token
    # stays the same).
    private def parseBoolean : AST::Boolean
      return AST::Boolean.new(@current)
    end

    # Parses the current series of tokens as `AST::Function`.
    private def parseFunction : AST::Function
      func = @current
      nextToken
      parameters = parseParameterList
      nextToken
      body = parseCompoundStatement

      return AST::Function.new(func, parameters, body)
    end

    # Parses the following comma separated list as Parameter List.
    private def parseParameterList : Array(AST::Identifier)
      list = [] of AST::Identifier
      expect(TokenType::LPAREN)
      nextToken

      return list if currentTokenIs(TokenType::RPAREN)

      expect(TokenType::IDENTIFIER)
      list << parseIdentifier
      while !nextTokenIs(TokenType::RPAREN)
        nextTokenExpect(TokenType::COMMA)
        nextTokenExpect(TokenType::IDENTIFIER)
        list << parseIdentifier
      end
      nextToken
      expect(TokenType::RPAREN)

      return list
    end

    # Parses a `let` statement.
    private def parseLetStatement : AST::LetStatement
      token = @current
      nextTokenExpect(TokenType::IDENTIFIER)
      name = parseIdentifier
      nextTokenExpect(TokenType::ASSIGN)
      nextToken
      value = parseExpression(Precedence::LOWEST)
      nextTokenExpect(TokenType::SEMICOLON)

      return AST::LetStatement.new(token, name, value)
    end

    # Parses a `return` statement.
    private def parseReturnStatement : AST::ReturnStatement
      token = @current
      nextToken

      if currentTokenIs(TokenType::SEMICOLON)
        return AST::ReturnStatement.new(token, nil)
      end

      value = parseExpression(Precedence::LOWEST)
      nextTokenExpect(TokenType::SEMICOLON)

      return AST::ReturnStatement.new(token, value)
    end

    # Parses an expression statement.
    private def parseExpressionStatement : AST::ExpressionStatement
      expr = parseExpression(Precedence::LOWEST)
      # HACK all expression statments end in a semi colon,
      # EXCEPT AST::Function and AST::IfExpression
      case expr
      when AST::Function, AST::IfExpression
        return AST::ExpressionStatement.new(expr)
      else
        nextTokenExpect(TokenType::SEMICOLON)
      end
      return AST::ExpressionStatement.new(expr)
    end

    # Parses a compound statement.
    private def parseCompoundStatement : AST::CompoundStatement
      expect(TokenType::LBRACE)
      openBrace = @current
      stmts = [] of AST::Statement
      nextToken

      while !currentTokenIs(TokenType::RBRACE) && !currentTokenIs(TokenType::EOF)
        stmt = parseStatement
        stmts << stmt
        nextToken
      end

      expect(TokenType::RBRACE)
      closeBrace = @current

      return AST::CompoundStatement.new(openBrace, stmts, closeBrace)
    end

    # Parses an expression given a *precedence*.
    private def parseExpression(precedence : Precedence) : AST::Expression
      raise ParserError.new(
        "Unexpected Token: #{@current}") if currentTokenIs(TokenType::SEMICOLON)

      prefixProc = @prefixParsers[@current.ttype]
      expr = prefixProc.call

      while !nextTokenIs(TokenType::SEMICOLON) && (precedence < nextPrecedence)
        if nextTokenIs(TokenType::EOF)
          raise ParserError.new("Unexpected EOF")
        end
        infixProc = @infixParsers.fetch(@next.ttype, nil)
        return expr if infixProc.nil?

        nextToken
        expr = infixProc.call(expr)
      end

      return expr
    end

    private def parsePrefixExpression : AST::PrefixExpression
      token = @current
      nextToken
      expr = parseExpression(Precedence::PREFIX)
      return AST::PrefixExpression.new(token, expr)
    end

    private def parseInfixExpression(left : AST::Expression) : AST::InfixExpression
      token = @current
      precedence = currentPrecedence
      nextToken
      right = parseExpression(precedence)
      return AST::InfixExpression.new(left, token, right)
    end

    private def parseGroupedExpression : AST::Expression
      nextToken
      expr = parseExpression(Precedence::LOWEST)
      nextTokenExpect(TokenType::RPAREN)
      return expr
    end

    private def parseIfExpression : AST::IfExpression
      token = @current
      nextTokenExpect(TokenType::LPAREN)
      nextToken
      condition = parseExpression(Precedence::LOWEST)
      nextTokenExpect(TokenType::RPAREN)

      nextTokenExpect(TokenType::LBRACE)
      trueBlock = parseCompoundStatement
      nextToken

      falseBlock = nil
      if currentTokenIs(TokenType::ELSE)
        nextTokenExpect(TokenType::LBRACE)
        falseBlock = parseCompoundStatement
      end

      return AST::IfExpression.new(token, condition, trueBlock, falseBlock)
    end

    private def parseCallExpression(function : AST::Expression) : AST::CallExpression
      case function
      when AST::Identifier, AST::Function
        args = parseExpressionList(TokenType::RPAREN)
        return AST::CallExpression.new(function, args)
      else
        raise ParserError.new("CallExpr expects Identifier or Function, " \
                              "but got #{function.class.name}")
      end
    end

    private def parseIndexExpression(expr : AST::Expression) : AST::IndexExpression
      token = @current
      nextToken
      index = parseExpression(Precedence::LOWEST)
      nextTokenExpect(TokenType::RBRACKET)
      return AST::IndexExpression.new(token, expr, index)
    end

    # Parses the following comma separated list until *stop_token* is encountered.
    # Used for CallExpressions and Arrays.
    private def parseExpressionList(stop_token : TokenType) : Array(AST::Expression)
      args = [] of AST::Expression
      nextToken

      return args if currentTokenIs(stop_token)

      args << parseExpression(Precedence::LOWEST)
      while nextTokenIs(TokenType::COMMA)
        nextToken
        nextToken
        args << parseExpression(Precedence::LOWEST)
      end
      nextTokenExpect(stop_token)

      return args
    end

    # Registers a prefix parser function *proc* for a given token type *ttype*.
    def registerPrefix(ttype : TokenType, proc : Proc(AST::Expression))
      @prefixParsers[ttype] = proc
    end

    # Registers a infix parser function *proc* for a given token type *ttype*.
    def registerInfix(ttype : TokenType, proc : InfixParseProc)
      @infixParsers[ttype] = proc
    end
  end
end
