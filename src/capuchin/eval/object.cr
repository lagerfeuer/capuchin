require "fnv"

module Capuchin
  enum ObjectType : Int8
    Error       = -1
    NULL        =  0
    Integer
    Boolean
    String
    Array
    Hash
    Function
    ReturnValue = 10
    Builtin
  end
end

module Capuchin
  class HashKey
    getter otype : ObjectType, value : UInt64

    def initialize(@otype : ObjectType, @value : UInt64)
    end

    def ==(other : HashKey)
      return (otype == other.otype) && (value == other.value)
    end

    def to_s(io)
      io << "#{otype}:#{value}"
    end
  end

  class HashPair
    getter key, value

    def initialize(@key : Capuchin::Object, @value : Capuchin::Object)
    end

    def to_s(io)
      io << "#{key.value}=#{value.value}"
    end
  end
end

module Capuchin
  module Const
    NULL  = Null.new
    TRUE  = Boolean.new(true)
    FALSE = Boolean.new(false)
  end

  abstract class Object
    abstract def otype : ObjectType
    abstract def audit : String
    abstract def value
    abstract def hash_key : HashKey
  end

  class Null < Object
    @otype : ObjectType
    getter otype : ObjectType

    def initialize
      @otype = ObjectType::NULL
    end

    def value : String
      return "(null)"
    end

    def audit : String
      return "(null)"
    end

    def hash_key : HashKey
      return HashKey.new(otype, 0)
    end
  end

  class Integer < Object
    @otype : ObjectType
    getter otype : ObjectType, value : Int64

    def initialize(@value : Int64)
      @otype = ObjectType::Integer
    end

    def audit : String
      return @value.to_s
    end

    def hash_key : HashKey
      return HashKey.new(otype, value.to_u64)
    end
  end

  class Boolean < Object
    @otype : ObjectType
    getter otype : ObjectType, value : Bool

    def initialize(@value : Bool)
      @otype = ObjectType::Boolean
    end

    def audit : String
      return @value.to_s
    end

    def hash_key : HashKey
      return HashKey.new(otype, (value ? 1 : 0).to_u64)
    end
  end

  class CString < Object
    @otype : ObjectType
    getter otype : ObjectType, value : String

    def initialize(@value : String)
      @otype = ObjectType::String
    end

    def audit : String
      return @value
    end

    def hash_key : HashKey
      fnv_digest = Digest::FNV64.digest(value)
      fnv_hash = IO::ByteFormat::BigEndian.decode(UInt64, fnv_digest)
      return HashKey.new(otype, fnv_hash)
    end
  end

  class CArray < Object
    @otype : ObjectType
    getter otype : ObjectType, elements : Array(Capuchin::Object)

    def initialize(@elements : Array(Capuchin::Object))
      @otype = ObjectType::Array
    end

    def value : String
      return "[ #{@elements.map(&.value.to_s).join(", ")} ]"
    end

    def audit : String
      return @value.map(&.to_s).join(", ")
    end

    def hash_key : HashKey
      raise "Cannot call #hash_key on Array Object"
    end
  end

  class CHash < Object
    @otype : ObjectType
    getter otype : ObjectType, elements : Hash(HashKey, HashPair)

    def initialize(@elements : Hash(HashKey, HashPair))
      @otype = ObjectType::Hash
    end

    def get(key : HashKey) : Object
      return Const::NULL unless elements.keys.includes?(key)
      return elements[key].value
    end

    def value : String
      return String.build do |str|
        str << "{" << LF
        @elements.each do |_, v|
          str << v.to_s << LF
        end
        str << "}" << LF
      end
    end

    def audit : String
      return String.build do |str|
        str << "{" << LF
        @elements.each do |k, v|
          str << "#{k}: #{v}" << LF
        end
        str << "}" << LF
      end
    end

    def hash_key : HashKey
      raise "Cannot call #hash_key on Hash Object"
    end
  end

  class Function < Object
    @otype : ObjectType
    getter otype : ObjectType, name : String, parameters : Array(AST::Identifier),
      body : AST::CompoundStatement, env : Environment

    def initialize(@parameters : Array(AST::Identifier),
                   @body : AST::CompoundStatement,
                   @env : Environment)
      @name = "<function>"
      @otype = ObjectType::Function
    end

    def value : String
      name
    end

    def audit : String
      return String.build do |str|
        str << "fun #{name}" << LF
        str << %(Params: #{@parameters.join(", ")}) << LF
      end
    end

    def hash_key : HashKey
      raise "Cannot call #hash_key on Function Object"
    end
  end

  class ReturnValue < Object
    @otype : ObjectType
    getter otype : ObjectType, value : Object

    def initialize(@value : Object)
      @otype = ObjectType::ReturnValue
    end

    def audit : String
      return "ReturnValue (#{@value.value})"
    end

    def hash_key : HashKey
      raise "Cannot call #hash_key on ReturnValue Object"
    end
  end

  class Builtin < Object
    @otype : ObjectType
    getter otype : ObjectType, value : String, num_args : UInt8, function : BuiltinFunction

    def initialize(@value : String, @num_args : UInt8, @function : BuiltinFunction)
      @otype = ObjectType::Builtin
    end

    def audit : String
      return "Builtin function <#{@value}>"
    end

    def hash_key : HashKey
      raise "Cannot call #hash_key on Builtin Object"
    end
  end

  class Error < Object
    @otype : ObjectType
    getter otype : ObjectType, message : String

    def initialize(@message : String)
      @otype = ObjectType::Error
    end

    def value : String
      return @message
    end

    def audit : String
      return "Error: #{@message}"
    end

    def self.create(message : String)
      return Error.new(message)
    end

    def hash_key : HashKey
      raise "Cannot call #hash_key on Error Object"
    end
  end
end
