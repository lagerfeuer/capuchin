require "./object.cr"

alias BuiltinFunctionOne = Proc(Capuchin::Object, Capuchin::Object)
alias BuiltinFunctionTwo = Proc(Capuchin::Object, Capuchin::Object, Capuchin::Object)
alias BuiltinFunction = BuiltinFunctionOne | BuiltinFunctionTwo

macro add_builtin(name, num_args, func)
  Builtins[{{name}}] = Builtin.new({{name}}, {{num_args}}.to_u8, {{func}})
end

module Capuchin
  def self.len(obj : Capuchin::Object) : Capuchin::Object
    case obj.otype
    when ObjectType::String
      return Integer.new(obj.as(CString).value.size.to_i64).as(Capuchin::Object)
    when ObjectType::Array
      return Integer.new(obj.as(CArray).elements.size.to_i64).as(Capuchin::Object)
    else
      raise EvalError.new("Undefined data type for 'len': #{obj.otype}")
    end
  end

  def self.first(obj : Capuchin::Object) : Capuchin::Object
    case obj.otype
    when ObjectType::Array
      array = obj.as(CArray)
      return Const::NULL if array.elements.size.zero?

      return array.elements.first
    else
      raise EvalError.new("Undefined data type for 'first': #{obj.otype}")
    end
  end

  def self.last(obj : Capuchin::Object) : Capuchin::Object
    case obj.otype
    when ObjectType::Array
      array = obj.as(CArray)
      return Const::NULL if array.elements.size.zero?

      return array.elements.last
    else
      raise EvalError.new("Undefined data type for 'last': #{obj.otype}")
    end
  end

  def self.rest(obj : Capuchin::Object) : Capuchin::Object
    case obj.otype
    when ObjectType::Array
      array = obj.as(CArray)
      return Const::NULL if array.elements.size.zero?

      return CArray.new(array.elements[1..])
    else
      raise EvalError.new("Undefined data type for 'rest': #{obj.otype}")
    end
  end

  def self.push(arr : Capuchin::Object, obj : Capuchin::Object) : Capuchin::Object
    case arr.otype
    when ObjectType::Array
      array = arr.as(CArray)
      return CArray.new(array.elements.dup << obj).as(Capuchin::Object)
    else
      raise EvalError.new("Undefined data type for 'push': #{arr.otype}")
    end
  end

  def self.output(obj : Capuchin::Object) : Capuchin::Object
    puts(obj.value)
    return Const::NULL.as(Capuchin::Object)
  end
end

module Capuchin
  Builtins = {} of String => Builtin
  add_builtin("print", 1, ->output(Object))
  add_builtin("len", 1, ->len(Object))
  add_builtin("first", 1, ->first(Object))
  add_builtin("last", 1, ->last(Object))
  add_builtin("rest", 1, ->rest(Object))
  add_builtin("push", 2, ->push(Object, Object))

  BuiltinsSource = <<-EOS
let map = fun(arr, f) {
  let iter = fun(arr, acc) {
    if (len(arr) == 0) {
      return acc;
    } else {
      iter(rest(arr), push(acc, f(first(arr))));
    }
  };
  iter(arr, []);
};

let reduce = fun(arr, initial, f) {
  let iter = fun(arr, acc) {
    if (len(arr) == 0) {
      return acc;
    } else {
      iter(rest(arr), f(acc, first(arr)));
    }
  };
  iter(arr, initial);
};

let sum = fun(arr) {
  return reduce(arr, 0, fun(start, e) { return start + e; });
};
EOS
end
