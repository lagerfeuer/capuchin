require "../ast/ast.cr"
require "./object.cr"
require "./environment.cr"
require "../errors.cr"

module Capuchin
  class Interpreter
    def initialize(@env : Environment)
    end

    def to_bool(b : Bool) : Boolean
      return b ? Const::TRUE : Const::FALSE
    end

    def error?(obj : Object) : Bool
      return (obj.otype == ObjectType::Error)
    end

    def unwrap(obj : Object) : Object
      return (obj.otype == ObjectType::ReturnValue) ? obj.as(ReturnValue).value : obj
    end

    def eval(ast : AST::AST, env : Environment = @env) : Object
      return eval(ast.statements, env)
    end

    def eval(stmts : Array(AST::Statement), env : Environment) : Object
      result : Object = Null.new
      stmts.each do |stmt|
        result = eval(stmt, env)

        return result.value.as(Object) if result.otype == ObjectType::ReturnValue

        return result if result.otype == ObjectType::Error
      end
      return result
    end

    def eval(node : AST::Node, env : Environment) : Object
      case node
      when AST::Identifier
        name = node.value
        value = env.get(name)
        return value unless value.nil?

        return Builtins[name] if Builtins.has_key?(name)

        raise EvalError.new("Unknown identifier '#{name}'")
      when AST::Integer
        return Integer.new(node.value)
      when AST::Boolean
        return node.value ? Const::TRUE : Const::FALSE
      when AST::CString
        return CString.new(node.value)
      when AST::CArray
        elements = eval_expressions(node.elements, env)
        return elements if elements.is_a?(Object) && error?(elements)
        return CArray.new(elements.as(Array(Capuchin::Object)))
      when AST::CHash
        return eval_hash(node, env)
      when AST::Function
        params = node.parameters
        body = node.body
        return Function.new(params, body, env)
      when AST::IfExpression
        return eval_if_expr(node, env)
      when AST::PrefixExpression
        right = eval(node.expr, env)
        return eval_prefix(node.token, right, env)
      when AST::InfixExpression
        left = eval(node.left, env)
        right = eval(node.right, env)
        return eval_infix(node.token, left, right, env)
      when AST::CallExpression
        function = eval(node.function, env)
        return function if error?(function)
        args = eval_expressions(node.arguments, env)
        return args if args.is_a?(Object) && error?(args)
        return apply_function(function, args.as(Array(Capuchin::Object)))
      when AST::IndexExpression
        expr = eval(node.expr, env)
        return expr if error?(expr)
        index = eval(node.index, env)
        return index if error?(index)
        return eval_index_expression(expr, index)
      when AST::CompoundStatement
        return eval_compound_stmt(node.stmts, env)
      when AST::ExpressionStatement
        return eval(node.expr, env)
      when AST::LetStatement
        value = eval(node.value, env)
        return value if error?(value)
        return env.set(node.name.value, value)
      when AST::ReturnStatement
        return ReturnValue
          .new((node.value.nil?) ? Const::NULL : eval(node.value.as(AST::Expression), env))
      else
        return Const::NULL
      end
    end

    def extend_function_env(function : Function, arguments : Array(Capuchin::Object)) : Environment
      env = Environment.enclosed_env(function.env)
      function.parameters.each_with_index do |param, idx|
        env.set(param.value, arguments[idx])
      end
      return env
    end

    def apply_function(object : Capuchin::Object, arguments : Array(Capuchin::Object)) : Capuchin::Object
      case object.otype
      when ObjectType::Function
        function = object.as(Function)
        extened_env = extend_function_env(function, arguments)
        result = eval(function.body, extened_env)
        return unwrap(result)
      when ObjectType::Builtin
        builtin = object.as(Builtin)
        raise EvalError.new(
          "'#{builtin.value}': expected #{builtin.num_args} argument, got #{arguments.size}"
        ) if arguments.size != builtin.num_args

        case builtin.function
        when BuiltinFunctionOne
          return builtin.function.as(BuiltinFunctionOne).call(arguments.first)
        when BuiltinFunctionTwo
          return builtin.function.as(BuiltinFunctionTwo).call(arguments.first, arguments[1])
        else
          raise EvalError.new("Mysterious error: #{__FILE__}:#{__LINE__}")
        end
      else
        raise EvalError.new("Not a function: #{object.otype}")
      end
    end

    def eval_expressions(exprs : Array(AST::Expression), env : Environment) : Array(Capuchin::Object) | Error
      converted = [] of Capuchin::Object
      return converted if exprs.empty?
      exprs.each do |expr|
        tmp = eval(expr, env)
        return tmp.as(Error) if error?(tmp)
        converted << tmp
      end
      return converted
    end

    def eval_hash(node : AST::CHash, env : Environment) : Capuchin::Object
      pairs = {} of HashKey => HashPair
      node.elements.each do |k, v|
        key = eval(k, env)
        return key if error?(key)
        val = eval(v, env)
        return val if error?(val)
        pairs[key.hash_key] = HashPair.new(key, val)
      end
      return CHash.new(pairs)
    end

    def eval_compound_stmt(stmts : Array(AST::Statement), env : Environment) : Object
      result : Object = Null.new
      stmts.each do |stmt|
        result = eval(stmt, env)

        return result if result.otype == ObjectType::ReturnValue ||
                         result.otype == ObjectType::Error
      end
      return result
    end

    def eval_prefix(operator : Token, right : Object, env : Environment) : Object
      return right if error?(right)

      case {right.otype, operator.ttype}
      when {_, TokenType::BANG}
        return self.not(right)
      when {ObjectType::Integer, TokenType::MINUS}
        return negate(right)
      else
        return Error.create(
          "Unknown operator: '%s' for type %s" % [operator.literal, right.otype])
      end
    end

    def eval_infix(operator : Token, left : Object, right : Object,
                   env : Environment) : Object
      return right if error?(right)
      return left if error?(left)

      case {left, right}
      when {Integer, Integer}
        case operator.ttype
        when TokenType::PLUS
          return Integer.new(left.value + right.value)
        when TokenType::MINUS
          return Integer.new(left.value - right.value)
        when TokenType::ASTERISK
          return Integer.new(left.value * right.value)
        when TokenType::SLASH
          return Integer.new((left.value / right.value).to_i64)
        when TokenType::LESS
          return to_bool(left.value < right.value)
        when TokenType::LESS_EQUAL
          return to_bool(left.value <= right.value)
        when TokenType::GREATER
          return to_bool(left.value > right.value)
        when TokenType::GREATER_EQUAL
          return to_bool(left.value >= right.value)
        when TokenType::EQUAL
          return to_bool(left.value == right.value)
        when TokenType::NOT_EQUAL
          return to_bool(left.value != right.value)
        else
          return Error.new(
            "Unknown operator: #{left.class.name} #{operator.literal} #{right.class.name}")
        end
      when {Boolean, Boolean}
        case operator.ttype
        when TokenType::EQUAL
          return to_bool(left.value == right.value)
        when TokenType::NOT_EQUAL
          return to_bool(left.value != right.value)
        else
          return Error.new(
            "Unknown operator: #{left.class.name} #{operator.literal} #{right.class.name}")
        end
      when {CString, CString}
        case operator.ttype
        when TokenType::PLUS
          return CString.new(left.value + right.value)
        else return Error.new(
          "Unknown operator: #{left.class.name} #{operator.literal} #{right.class.name}")
        end
      else
        return Error.new(
          "Type mismatch: #{left.class.name} #{operator.literal} #{right.class.name}")
      end
    end

    def eval_if_expr(expr : AST::IfExpression, env : Environment) : Object
      condition = eval(expr.condition, env)
      return condition if error?(condition)

      if truthy(condition) == Const::TRUE
        return eval(expr.trueStmt, env)
      elsif !expr.falseStmt.nil?
        return eval(expr.falseStmt.as(AST::Statement), env)
      end
      return Const::NULL
    end

    def eval_index_expression(expr : Object, index : Object) : Object
      case {expr, index}
      when {CArray, Integer}
        return eval_array_access(expr, index)
      when {CHash, _}
        return eval_hash_access(expr, index)
      else
        return Error.new(
          "Index operator not supported for '#{expr.otype}[#{index.otype}]'")
      end
    end

    def eval_array_access(array : CArray, index : Integer) : Object
      idx = index.value
      max = array.elements.size
      return Const::NULL if idx < 0 || idx >= max

      return array.elements[idx]
    end

    def eval_hash_access(hash : CHash, index : Object) : Object
      key = index.hash_key
      return hash.get(key)
    end

    def truthy(obj : Object) : Object
      case obj.otype
      when ObjectType::Boolean
        return (obj == Const::TRUE) ? Const::TRUE : Const::FALSE
      when ObjectType::Integer
        return (obj.value == 0) ? Const::FALSE : Const::TRUE
      when ObjectType::NULL
        return Const::FALSE
      else
        return Const::TRUE
      end
    end

    def not(obj : Object) : Object
      return (truthy(obj) == Const::TRUE) ? Const::FALSE : Const::TRUE
    end

    def negate(obj : Object) : Object
      case obj.otype
      when ObjectType::Integer
        return Integer.new(-obj.as(Integer).value)
      else
        return Error.new(
          "Unknown operator: '-' for type %s" % [obj.otype])
      end
    end
  end
end
