require "object.cr"

module Capuchin
  class Environment
    @outer : Environment | Nil
    property outer

    def self.enclosed_env(outer : Environment) : Environment
      env = Environment.new
      env.outer = outer
      return env
    end

    def initialize
      @env = {} of String => Capuchin::Object
      @outer = nil
    end

    def get(name : String) : Capuchin::Object | Nil
      result = @env.fetch(name, nil)
      if result.nil? && !@outer.nil?
        result = @outer.as(Environment).get(name)
      end
      return result
    end

    def set(name : String, value : Capuchin::Object) : Capuchin::Object
      @env[name] = value
      return value
    end

    def to_s(io)
      io << @env.to_s << LF
      unless @outer.nil?
        io << "Outer: " << @outer.to_s << LF
      end
    end
  end
end
