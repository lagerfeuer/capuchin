require "./node.cr"
require "./literals.cr"
require "./expression.cr"

module Capuchin
  module AST
    abstract class Statement < Node
      # Returns the current statement as source code string.
      abstract def source_code : String
    end

    # Container statement for multiple statements, like in `if` expressions.
    # Requires '{' and '}'.
    #
    # **Examples**
    # if (a < b) {
    #   let a = 1;
    #   let b = 2;
    # }
    class CompoundStatement < Statement
      getter open, stmts, close

      def initialize(@open : Token, @stmts : Array(Statement), @close : Token)
      end

      def to_s(io)
        @stmts.each { |stmt| stmt.to_s(io) }
      end

      def source_code : String
        return String.build do |str|
          str << "{" << LF
          str << @stmts.map(&.source_code).join(LF)
          str << "}"
        end
      end

      def audit(indent) : String
        return @stmts.map(&.audit(indent)).join(LF)
      end
    end

    # Expression statement.
    #
    # **Examples**
    # ```
    # 1 + 2
    # var * 3
    # (var == true)
    # ```
    class ExpressionStatement < Statement
      @token : Token
      getter token, expr

      def initialize(@expr : Expression)
        @token = @expr.token
      end

      def to_s(io)
        io << @expr.to_s
      end

      def source_code : String
        return "#{self};"
      end

      def audit(indent) : String
        return @expr.audit(indent)
      end
    end

    # Let statement.
    #
    # **Examples**
    # ```
    # let a = 5
    # let b = a + 3
    # let c = add(1, 2)
    # ```
    class LetStatement < Statement
      getter name : Identifier
      getter value : Expression

      def initialize(@token : Token, @name : Identifier, @value : Expression)
      end

      def to_s(io)
        io << "let #{@name} = #{@value}"
      end

      def source_code : String
        return "#{self};"
      end

      def audit(indent) : String
        return [
          super(indent),
          @name.audit(indent + 1),
          @value.audit(indent + 1),
        ].join(LF)
      end
    end

    # Return statement.
    #
    # **Examples**
    # ```
    # return 5
    # return (a + 3) * 2
    # return add(1, 2)
    # ```
    class ReturnStatement < Statement
      getter value : Expression | Nil

      def initialize(@token : Token, @value : Expression | Nil)
      end

      def to_s(io)
        io << "return #{@value}".chomp(' ')
      end

      def source_code : String
        return "#{self};"
      end

      def audit(indent) : String
        return super(indent) if @value.nil?
        return [
          super(indent),
          @value.as(Expression).audit(indent + 1),
        ].join(LF)
      end
    end
  end
end
