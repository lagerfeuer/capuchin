require "./expression.cr"
require "./statement.cr"

module Capuchin
  module AST
    abstract class Literal < Expression
    end

    # Represents an Identifier in the AST.
    class Identifier < Literal
      @value : String
      getter value, token

      def initialize(@token : Token)
        @value = @token.literal
      end

      def to_s(io)
        io << @value
      end

      def info : String
        to_s
      end

      def audit(indent) : String
        return "#{self.class.name} <#{@value}>".indent(indent)
      end
    end

    # Represents an Integer Literal in the AST.
    class Integer < Literal
      @value : Int64
      getter value, token

      def initialize(@token : Token)
        @value = @token.literal.to_i64
      end

      def to_s(io)
        io << @value.to_s
      end

      def info : String
        to_s
      end

      def audit(indent) : String
        return "#{self.class.name} <#{@value}>".indent(indent)
      end
    end

    # Represents a Boolean Literal in the AST.
    class Boolean < Literal
      @value : Bool
      getter value, token

      def initialize(@token : Token)
        @value = (@token.ttype === TokenType::TRUE)
      end

      def to_s(io)
        io << @value.to_s
      end

      def info : String
        to_s
      end

      def audit(indent) : String
        return "#{self.class.name} <#{@value}>".indent(indent)
      end
    end

    # Represents a String Literal in the AST.
    class CString < Literal
      @value : String
      getter value, token

      def initialize(@token : Token)
        @value = @token.literal[1...-1] # remove '"' from string
      end

      def to_s(io)
        io << @value
      end

      def info : String
        @value
      end

      def audit(indent) : String
        return "#{self.class.name} #{@value}".indent(indent)
      end
    end

    # Represents an Array Literal in the AST.
    class CArray < Literal
      getter elements : Array(Expression), open : Token, close : Token

      def initialize(@open : Token, @elements : Array(Expression), @close : Token)
      end

      def to_s(io)
        io << "array ["
        io << @elements.join(", ")
        io << "]" << LF
      end

      def token : Token
        return @open
      end

      def info : String
        return %(<Array> (#{@elements.join(", ")}))
      end

      def audit(indent) : String
        return %(#{self.class.name} [ #{@elements.join(", ")} ])
      end
    end

    # Represents a Hash Map Literal in the AST.
    class CHash < Literal
      getter elements : Hash(Expression, Expression), open : Token, close : Token

      def initialize(@open : Token, @elements : Hash(Expression, Expression),
                     @close : Token)
      end

      def to_s(io)
        io << "hash {"
        io << @elements.map { |k, v| "#{k}=#{v}" }.join(", ")
        io << "}" << LF
      end

      def token : Token
        return @open
      end

      def info : String
        return %(<Hash> (#{@elements.map { |k, v| "#{k}=#{v}" }.join(", ")}))
      end

      def audit(indent) : String
        return %(#{self.class.name} [ #{@elements.map { |k, v| "#{k}=#{v}" }.join(", ")} ])
      end
    end

    # Represents a Function Literal in the AST.
    class Function < Literal
      getter token, parameters, body

      def initialize(@token : Token,
                     @parameters : Array(Identifier),
                     @body : CompoundStatement)
      end

      def parameters_str : String
        return @parameters.map(&.to_s).join(", ")
      end

      def to_s(io)
        io << "fun (" << parameters_str << ")" << LF
        io << @body.to_s
      end

      def info : String
        return "fun (#{parameters_str})"
      end

      def audit(indent) : String
        return "#{self.class.name} ( #{parameters_str} )"
      end
    end
  end
end
