require "../utils.cr"

module Capuchin
  module AST
    # Base class for all AST Nodes.
    # All nodes must implement *Node#to_s* and *Node#audit*.
    abstract class Node
      abstract def to_s(io)

      # Returns a description of the current node.
      def audit(indent) : String
        return " " * indent + self.class.name.to_s
      end
    end
  end
end
