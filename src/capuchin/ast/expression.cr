require "./node.cr"

module Capuchin
  module AST
    # Represents an Expression in the AST.
    abstract class Expression < Node
      abstract def info
    end

    # Represents an expression prefixed with an operator.
    class PrefixExpression < Expression
      getter operator, expr

      def initialize(@operator : Token, @expr : Expression)
      end

      def token
        operator
      end

      def to_s(io)
        io << "#{operator.literal}#{expr}"
      end

      def info : String
        return "<PrefixExpr> '#{operator.literal}' <#{@expr}>"
      end

      def audit(indent) : String
        return [
          "#{super(indent)} '#{operator.literal}'",
          @expr.audit(indent + 1),
        ].join(LF)
      end
    end

    # Represents an expression prefixed with an operator.
    class InfixExpression < Expression
      getter left, operator, right

      def initialize(@left : Expression, @operator : Token, @right : Expression)
      end

      def token
        operator
      end

      def to_s(io)
        io << "#{@left} #{operator.literal} #{@right}"
      end

      def info : String
        return "<InfixExpr> '#{operator.literal}' <#{@left}> <#{@right}>"
      end

      def audit(indent) : String
        return [
          "#{super(indent)} '#{operator.literal}'",
          @left.audit(indent + 1),
          @right.audit(indent + 1),
        ].join(LF)
      end
    end

    # Represents an if expression, which my be use as statement
    # inside an `ExpressionStatement` or inline, like:
    # ```
    # a = if (b > 3) { true; } else { false; }
    class IfExpression < Expression
      getter token, condition, trueStmt, falseStmt

      def initialize(@token : Token,
                     @condition : Expression,
                     @trueStmt : Statement,
                     @falseStmt : Statement | Nil)
      end

      def to_s(io)
        io << "<if> {" << LF << trueStmt.to_s << LF << "}" << LF
        unless @falseStmt.nil?
          io << "<else> {" << LF << falseStmt.to_s << LF << "}" << LF
        end
      end

      def info : String
        return "<IfExpr> '#{condition}' [\n#{trueStmt}\n] [\n#{falseStmt}\n]"
      end

      def audit(indent) : String
        ifStmt = [
          "#{super(indent)} (#{condition})",
          @trueStmt.audit(indent + 1),
        ]
        unless @falseStmt.nil?
          # HACK otherwise throws a compile error without the `as`
          ifStmt << @falseStmt.as(CompoundStatement).audit(indent + 1)
        end
        return ifStmt.join(LF)
      end
    end

    # Represents a call expression.
    class CallExpression < Expression
      @token : Token
      getter token, function, arguments

      def initialize(@function : Identifier | Function,
                     @arguments : Array(Expression))
        @token = @function.token
      end

      def arguments_str : String
        return arguments.map(&.to_s).join(",")
      end

      def to_s(io)
        io << "<call> #{function} (" << arguments_str << ")"
      end

      def info : String
        return "<CallExpr> #{function} ( #{arguments_str} )"
      end

      def audit(indent) : String
        return "#{super(indent)} <#{function.audit(indent + 1)}>" \
               "(#{arguments_str})"
      end
    end

    # Represents an index operation on an Array.
    class IndexExpression < Expression
      getter token, expr, index

      def initialize(@token : Token, @expr : Expression, @index : Expression)
      end

      def to_s(io)
        io << "<IndexExpr> #{@expr}[#{@index}]"
      end

      def info : String
        return "<IndexExpr> #{@expr}[#{@index}]"
      end

      def audit(indent) : String
        return "#{super(indent)} #{@expr.audit(indent + 1)}[#{index.audit(0)}]"
      end
    end
  end
end
