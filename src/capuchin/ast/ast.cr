require "../token/token.cr"
require "./*"

module Capuchin
  module AST
    # Represents the entire Abstract Syntax Tree.
    class AST
      getter statements : Array(Statement)

      def initialize(@statements : Array(Statement))
      end

      # Appends a Statement to the AST.
      def <<(stmt : Statement) : Nil
        @statements << stmt
      end

      def to_s(io)
        @statements.each(&.to_s(io))
      end

      # Inspect and audit all statements.
      def audit : String
        return @statements.map do |stmt|
          stmt.audit(0)
        end.join(LF)
      end
    end
  end
end

class String
  def indent(size)
    return " " * size + self
  end
end
