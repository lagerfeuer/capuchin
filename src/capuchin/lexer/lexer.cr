module Capuchin
  # :nodoc:
  KEYWORD_TOKEN_MAP = {
    "let"    => TokenType::LET,
    "fun"    => TokenType::FUNCTION,
    "return" => TokenType::RETURN,
    "if"     => TokenType::IF,
    "else"   => TokenType::ELSE,
    "true"   => TokenType::TRUE,
    "false"  => TokenType::FALSE,
  }

  # Lexer class to translate source code into tokens.
  class Lexer
    # TODO call `#getTokens` on input and simply work with the `Array(Token)`?
    # Higher initialization time but faster access.
    def initialize(@source : String)
      @pos = -1
      @current = Char::ZERO
      readChar
    end

    # Get all tokens as Array.
    def getTokens : Array(Token)
      tokens = [] of Token
      while (tok = nextToken).ttype != TokenType::EOF
        tokens << tok
      end
      return tokens
    end

    # Read the next token.
    # Returns the token, the EOF token (if the end was reached), or
    # the INVALID token (if an unknown or unexpected character was found).
    def nextToken : Token
      consumeWhitespace # eat all whitespaces

      ttype : TokenType = TokenType::INVALID
      literal = @current.to_s

      case @current
      when '='
        case peekChar
        when '='
          ttype = TokenType::EQUAL
          literal = @current.to_s + readChar
        else
          ttype = TokenType::ASSIGN
        end
      when '+' then ttype = TokenType::PLUS
      when '-' then ttype = TokenType::MINUS
      when '*' then ttype = TokenType::ASTERISK
      when '/' then ttype = TokenType::SLASH
      when '!'
        case peekChar
        when '='
          ttype = TokenType::NOT_EQUAL
          literal = @current.to_s + readChar
        else
          ttype = TokenType::BANG
        end
      when ',' then ttype = TokenType::COMMA
      when ';' then ttype = TokenType::SEMICOLON
      when ':' then ttype = TokenType::COLON
      when '(' then ttype = TokenType::LPAREN
      when ')' then ttype = TokenType::RPAREN
      when '[' then ttype = TokenType::LBRACKET
      when ']' then ttype = TokenType::RBRACKET
      when '{' then ttype = TokenType::LBRACE
      when '}' then ttype = TokenType::RBRACE
      when '<'
        case peekChar
        when '='
          ttype = TokenType::LESS_EQUAL
          literal = @current.to_s + readChar
        else
          ttype = TokenType::LESS
        end
      when '>'
        case peekChar
        when '='
          ttype = TokenType::GREATER_EQUAL
          literal = @current.to_s + readChar
        else
          ttype = TokenType::GREATER
        end
      when '"'
        ttype = TokenType::STRING
        literal = readString
        return Token.new(ttype, literal)
      when Char::ZERO
        ttype = TokenType::EOF
      when .alphanumeric?
        case @current
        when .letter?
          ttype = TokenType::IDENTIFIER
          literal = readIdentifier
        when .number?
          ttype = TokenType::INTEGER
          literal = readNumber
        else
          raise LexerError.new("Cannot create token from '#{@current}'")
        end
        return createKeywordToken(literal) if isKeyword(literal)
        return Token.new(ttype, literal)
      else ttype = TokenType::INVALID # TODO raise error when invalid
      end

      readChar
      return Token.new(ttype, literal)
    end

    # Returns the next character in *source*.
    # **Does not** advance the reader position.
    private def peekChar : Char
      return Char::ZERO if (@pos + 1) >= @source.size
      return @source[@pos + 1]
    end

    # Reads the next character in *source*.
    # Returns the charactor or `Char::ZERO` if the end was reached.
    private def readChar : Char
      if (@pos + 1) >= @source.size
        return (@current = Char::ZERO)
      end

      @pos += 1
      @current = @source[@pos]
      return @current
    end

    # Read the following characters as identifier,
    # until a non-valid character is found.
    # Returns the identifier as String.
    private def readIdentifier : String
      identifier = [@current]
      while isValidChar(readChar, TokenType::IDENTIFIER)
        identifier << @current
      end
      return identifier.join("")
    end

    # Read the following characters as string,
    # until a closing \" is found.
    # Returns the string as Crystal String.
    private def readString : String
      literal = String.build do |str|
        str << @current
        while isValidChar(readChar, TokenType::STRING)
          str << @current
        end
        if @current === '"'
          str << @current
          readChar
        end
      end
      return literal
    end

    # Read the following characters as number,
    # until a non-valid character is found.
    # Returns the number as String.
    #
    # TODO support float and hex number notation
    private def readNumber : String
      integer = [@current]
      while isValidChar(readChar, TokenType::INTEGER)
        integer << @current
      end
      return integer.join("")
    end

    # Takes a char and a `TokenType` and checks the validity,
    # i.e. whether this character can be part of a `TokenType` literal.
    # Returns true or false.
    private def isValidChar(c : Char, t : TokenType) : Bool
      return false if c === Char::ZERO

      case t
      when TokenType::STRING
        return c != '"'
      when TokenType::IDENTIFIER
        return c.alphanumeric? || c === '_'
      when TokenType::INTEGER
        return c.number?
        # when TokenType::REAL
        #   return c.number? || c === '.'
      else
        raise NotImplementedError.new(
          "No valid character check available for #{t}")
      end
    end

    # Check if *word* is a keyword.
    # Returns true or false.
    private def isKeyword(word : String) : Bool
      return KEYWORD_TOKEN_MAP.keys.includes?(word)
    end

    # Returns a token created with *word*.
    private def createKeywordToken(word : String) : Token
      return Token.new(KEYWORD_TOKEN_MAP[word], word)
    end

    private def consumeWhitespace
      while @current.whitespace?
        readChar
      end
    end
  end
end
