module Capuchin
  class LexerError < Exception
  end

  class ParserError < Exception
  end

  class EvalError < Exception
  end

  class EnvironmentError < Exception
  end
end
