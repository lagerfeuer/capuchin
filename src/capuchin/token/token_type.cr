module Capuchin
  enum TokenType : Int8
    INVALID = -1
    EOF     =  0

    IDENTIFIER = 10
    INTEGER
    REAL
    STRING

    ASSIGN   = 20
    PLUS
    MINUS
    ASTERISK
    SLASH
    BANG

    EQUAL         = 30
    NOT_EQUAL
    LESS
    LESS_EQUAL
    GREATER
    GREATER_EQUAL

    COMMA     = 40
    SEMICOLON
    COLON

    LPAREN   = 50
    RPAREN
    LBRACKET
    RBRACKET
    LBRACE
    RBRACE

    FUNCTION = 100
    LET
    IF
    ELSE
    RETURN
    TRUE
    FALSE

    def isBinaryOperator
      return ((self >= PLUS) && (self <= SLASH))
    end

    def isComparisonOperator
      return ((self >= EQUAL) && (self <= GREATER_EQUAL))
    end
  end
end
