require "./token_type.cr"

module Capuchin
  # Class representing a single token, consisting
  # of `TokenType` and literal.
  #
  # TODO Add file, line and column information.
  class Token
    @literal : String
    getter ttype : TokenType, literal : String

    def initialize(@ttype : TokenType, literal : String | Nil)
      @literal = (literal.nil?) ? "(nil)" : literal
    end

    def to_s(io)
      io << "Token(#{@ttype}, #{@literal})"
    end
  end
end
