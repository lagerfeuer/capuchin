require "./capuchin.cr"
require "fancyline"
require "cli"

module Capuchin
  class Cli < Cli::Command
    version "#{Capuchin::VERSION}"

    class Help
      header "Capuchin - A Monkey Interpreter in Crystal"
      footer "Written by Crystal, the famous Capuchin"
    end

    class Options
      # TODO add "tracer mode", where we output all tokens according to the lexer.
      bool "--trace", desc: "Print parser debug information", default: false
      arg "file", required: false
      help
    end

    def run
      if args.file?
        puts "script"
        exec_script(args.file)
      else
        puts "repl"
        repl()
      end
    end
  end
end

macro create_interpreter
  Capuchin::Interpreter.new(Capuchin::Environment.new)
end

macro init_interpreter(interpreter)
  # load Capuchin builtins
  {{interpreter}}.eval(
    Capuchin::Parser.new(
    Capuchin::Lexer.new(Capuchin::BuiltinsSource)).parse)
end

# Start the Monkey REPL
def repl : Nil
  puts "Capuchin #{Capuchin::VERSION}"
  puts "Press Ctrl-C or Ctrl-D to quit."
  puts "Or enter 'quit' or 'exit'."
  puts

  capuchin = create_interpreter
  init_interpreter(capuchin)
  fancy = Fancyline.new

  while input = fancy.readline("$> ")
    begin
      # exit if exit keywords
      return 0 if %w[quit exit].any? { |kw| kw === input }

      # REPL
      unless input.empty?
        lexer = Capuchin::Lexer.new(input)
        parser = Capuchin::Parser.new(lexer)
        ast = parser.parse
        result = capuchin.eval(ast)
        puts result.value.to_s
      end

      # print ParserErrors and continue
    rescue err : Capuchin::ParserError | Capuchin::EvalError
      puts err
    end
  end
rescue err : Fancyline::Interrupt
  puts "Exiting..."
end

# Execute *filename* as Monkey script
def exec_script(filename : String) : Nil
  capuchin = create_interpreter
  init_interpreter(capuchin)
  script = File.read(filename)
  lexer = Capuchin::Lexer.new(script)
  parser = Capuchin::Parser.new(lexer)
  ast = parser.parse
  capuchin.eval(ast)
end

def main
  Capuchin::Cli.run(ARGV)
end

main
