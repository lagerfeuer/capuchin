# Capuchin

The name is inspired by [Crystal the Monkey](https://en.wikipedia.org/wiki/Crystal_the_Monkey).

## Installation

TODO: Write installation instructions here

## Usage
```sh
make
./out/capuchin
```

Example: `./out/capuchin example/example.monkey`

### Docker
```sh
docker build -t capuchin:latest .
docker run -it capuchin:latest
```

## TODO
* [x] Fix `Interpreter` class: everything is static right now, refactor to use it as `Interpreter.new(Environment)`
* [x] Add basic CLI
* [ ] Implement proper comparison for all AST, Statements, Expressions and Literals in `spec/`
* [ ] Fix `ameba` errors (mostly coding convetion stuff)
* [ ] Rename all statments to e.g. `LetStmt` (now: `LetStatement`)
* [ ] Rename all occurences of `Object` (Crystal already has an `Object` class)
* [ ] Rename all expression to e.g. `CallExpr` (now: `CallExpression`)
* [ ] Rename `TokenType` to `Type`
