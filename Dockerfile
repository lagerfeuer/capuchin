FROM crystallang/crystal:0.34.0-alpine-build AS compile
WORKDIR /build
COPY . /build
RUN crystal build --release --no-debug --static src/main.cr -o capuchin.out

FROM alpine:latest
WORKDIR /root/
COPY --from=compile /build/capuchin.out capuchin
CMD ["./capuchin"]
